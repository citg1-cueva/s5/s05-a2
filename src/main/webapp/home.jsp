<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>JOB FINDER| Home</title>
	</head>
	<body>
		<%
			String message = "";
			String type = session.getAttribute("regType").toString();
			String name = session.getAttribute("fullname").toString();
			if(type.equals("Applicant")){
				message = "Welcome Applicant! You may NOW start looking for Career opportunity ";
			}
			else{
				message = "Welcome Employer! You may NOW start hiring and giving out contracts ";
			}
		%>
		
		<h1>Welcome <%= name %>!</h1>
		<p><%= message %></p>
	</body>
</html>