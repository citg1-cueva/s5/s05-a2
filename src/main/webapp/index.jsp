<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
		<meta charset="UTF-8">
		<title>JOB FINDER</title>
		<style>
			div {
				margin-top: 5px;
				margin-bottom: 5px;
			}
			
			#container{
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
			div > input, div > select, div > textarea{
				width: 96%;
			}
			
		</style>
	</head>
	<body>
		
		
		<div id="container">
		<h1>Servlet Job Finder</h1>
		
			<form action="register" method="post">
				<!-- Name -->
				<div>
					<label for="fname">First Name</label>
					<input type="text" name="fname" required>
				</div>
				<div>
					<label for="lname">Last Name</label>
					<input type="text" name="lname" required>
				</div>
			
				
				<!-- Phone Number -->
				<div>
					<label for="phone">Phone Number</label>
					<input type="tel" name="phone" required>
				</div>
				
				<!-- email -->
				<div>
					<label for="email">Email</label>
					<input type="email" name="email" required>
				</div>
				
				<!-- Add discovery -->
				<fieldset>
					<legend>How did you discover the app?</legend>
					<!-- Friends -->
					<input type="radio" id="friends" name="discovery" value="Friends" required>
					<label for="friends">Friends</label>
					
					<!-- Social Media -->
					<input type="radio" id="socialMedia" name="discovery" value="Social Media" required>
					<label for="socialMedia">Social Media</label>
					
					<!-- Other -->
					<input type="radio" id="others" name="discovery" value="others" required>
					<label for="others">Other</label>
					
					
				</fieldset>
				
					
					<!--  Date of birth -->
					<div>
						<label for="dob"> Date of birth</label>
						<input type="date" name="dob" required>
					</div>
					
					<!-- Type of Entry -->
					<div>
						<label for="regType">Are you and Employer or Applicant</label>
						<select id="type" name="regType">
							<option value="" selected="selected">Select one</option>
							<option value="Applicant">Applicant</option>
							<option value="Employer">Employer</option>
						</select>
					</div>
					
					
					<!-- Profile Description -->
					<div>
						<label for="profile">Profile Description</label>
						<textarea name="profile" maxlength="500"></textarea>
					</div>
					
					<!-- button for submit -->
					
					<button>Register</button>
					
			</form>
		</div>
	</body>
</html>