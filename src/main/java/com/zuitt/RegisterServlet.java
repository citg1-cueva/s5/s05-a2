package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;
	public void init() throws ServletException{
		System.out.println("RegisterServlet has been initialized.");
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		PrintWriter out = res.getWriter();
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String discovery = req.getParameter("discovery");
		String dob = req.getParameter("dob");
		String regType = req.getParameter("regType");
		String profile = req.getParameter("profile");
		
		HttpSession session = req.getSession();
		
		session.setAttribute("fname", fname.substring(0, 1).toUpperCase()+ fname.substring(1));
		session.setAttribute("lname", lname.substring(0, 1).toUpperCase()+ lname.substring(1));
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("discovery", discovery);
		session.setAttribute("dob", dob);
		session.setAttribute("regType", regType);
		session.setAttribute("profile", profile);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("RegisterServlet has been finalized");
	}
	
	

}
